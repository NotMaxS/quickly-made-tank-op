// CBA Settings
// Uncomment lines if you want to change the default values
// Values that can be defined by lobby parameters are not present in this file

// ACE Logistics
// force ace_rearm_level = 0;
force ace_repair_engineerSetting_fullRepair = 1;
force ace_repair_engineerSetting_repair = 0;
force ace_repair_fullRepairLocation = 1;
force ace_repair_timeCoefficientFullRepair = 0.15;
force ace_repair_repairDamageThreshold_engineer = 0;
force ace_repair_miscRepairTime = 25;
// force ace_repair_repairDamageThreshold = 0.6;
// force ace_repair_wheelRepairRequiredItems = 0;
 force ace_spectator_enableAI = true;
 force ace_viewdistance_limitViewDistance = 2000;

// ACE Pylons
// force ace_pylons_enabledForZeus = true;
// force ace_pylons_enabledFromAmmoTrucks = true;
// force ace_pylons_rearmNewPylons = false;
// force ace_pylons_requireEngineer = false;
// force ace_pylons_requireToolkit = true;
// force ace_pylons_searchDistance = 15;
// force ace_pylons_timePerPylon = 5;

