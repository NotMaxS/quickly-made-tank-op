// SXP_fnc_setupVehicle
// Handles setting up vehicles at mission start, and upon respawn

// Only run on the server
if (!isServer) exitWith {};

// Define variables
_this params [
	["_newVeh", objNull, [objNull]],
	["_oldVeh", objNull, [objNull]],
	["_start", false, [true]]
];

// If newVeh is nil, exit the script
if (isNil "_newVeh") exitWith {};

// Start our switch block, check the vehicle classname to determine what needs to be done for setup
switch (toLower (typeOf _newVeh)) do {

	
	//Kuma
	case (toLower "I_MBT_03_cannon_F"): {

	//load inventory 	
	[_newVeh, "kuma"] call XPT_fnc_loadItemCargo;

	//disable thermals
	_newVeh disableTIEquipment true;
	
	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;

	// replace camo with RACS version
	_newVeh setObjectTexture [0, "media\textures\mbt_03_ext01_desert_co.paa"];
	_newVeh setObjectTexture [1, "media\textures\mbt_03_ext02_desert_co.paa"];
	_newVeh setObjectTexture [2, "media\textures\mbt_03_rcws_desert_co.paa"];
	_newVeh setObjectTexture [3, "a3\armor_f\data\camonet_aaf_digi_desert_co.paa"]

	};

	//Autocannon Nyx
	case (toLower "I_LT_01_cannon_F"): {

	//load inventory 	
	[_newVeh, "kuma"] call XPT_fnc_loadItemCargo;

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;
	_newVeh setVehicleReportRemoteTargets true; 
	_newVeh setVehicleReceiveRemoteTargets true; 

	_newVeh setObjectTexture [0, "media\textures\lt_01_main_desert_co.paa"];
	_newVeh setObjectTexture [1, "media\textures\lt_01_at_desert_co.paa"];
	_newVeh setObjectTexture [2, "a3\armor_f\data\camonet_aaf_digi_desert_co.paa"];
	};

	//HAT Nyx
	case (toLower "TMTM_I_LT_01_HAT_F"): {

	//load inventory 	
	[_newVeh, "reconNyx"] call XPT_fnc_loadItemCargo;

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;
	_newVeh setVehicleReportRemoteTargets true; 
	_newVeh setVehicleReceiveRemoteTargets true; 

	_newVeh setObjectTexture [0, "media\textures\lt_01_main_desert_co.paa"];
	_newVeh setObjectTexture [1, "media\textures\lt_01_at_desert_co.paa"];
	_newVeh setObjectTexture [2, "a3\armor_f\data\camonet_aaf_digi_desert_co.paa"];
	};

	//Recon Nyx
	case (toLower "I_LT_01_scout_F"): {

	//load inventory 	
	[_newVeh, "reconNyx"] call XPT_fnc_loadItemCargo;

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;
	_newVeh setVehicleReportRemoteTargets true; 
	_newVeh setVehicleReceiveRemoteTargets true; 

	_newVeh setObjectTexture [0, "media\textures\lt_01_main_desert_co.paa"];
	_newVeh setObjectTexture [1, "media\textures\lt_01_at_desert_co.paa"];
	_newVeh setObjectTexture [2, "a3\armor_f\data\camonet_aaf_digi_desert_co.paa"];
	};

	//Logistics vehicle, set up it's itemCargos
	case (toLower "B_D_APC_Tracked_01_rcws_lxWS"): {

	//load inventory 	
	[_newVeh, "logiVehicle"] call XPT_fnc_loadItemCargo;

	//disable thermals
	_newVeh disableTIEquipment true;

	// Enable datalink
	_newVeh setVehicleReportOwnPosition true;

	//Spectate hold action that actually just kills you and hides the body
			[     
			 _newVeh,              
			 "Extract",           
			 "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",    
			 "\a3\ui_f\data\IGUI\Cfg\holdactions\holdAction_connect_ca.paa",      
			 "player distance _target < 3",         
			 "player distance _target < 3",         
			 {},               
			 {},              
			 {player setDamage 1;},      
			 {},             
			 [],                 
			 5,                
			 0,                
			 true,              
			 false             
		] remoteExec ["BIS_fnc_holdActionAdd", 0, _newVeh];
	};
};