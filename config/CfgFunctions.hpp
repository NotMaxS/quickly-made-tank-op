// Functions library
// Defines custom functions for the mission. Anything that needs to be called more than once should be a function
// https://community.bistudio.com/wiki/Functions_Library_(Arma_3)

//Function for vehicle setup 
class SXP 
{
	class vehicle 
	{
		class setupVehicle {}; 
	};

}; 

//Function for improved spectator
class FR 
{
	class feature_spectator 
	{
		class spectator_setupSpectator {}; 
	};
}; 