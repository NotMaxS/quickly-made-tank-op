// XPTloadouts.hpp
// Used for defining advanced respawn loadouts for players
// Default behaviour is to check if the player unit has a special loadout defined. Otherwise, it will check to see if the classname matches a loadout
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class loadouts
{	
	#define UNIFORM_TANK "U_I_C_Soldier_Para_4_F", "U_I_C_Soldier_Para_3_F", "CUP_I_B_PARA_Unit_8", "U_I_E_Uniform_01_tanktop_F", "U_B_CTRG_Soldier_2_Arid_F", "U_lxWS_B_CombatUniform_desert_tshirt", "CUP_U_CRYE_TAN_Roll", "tmtm_u_guerillaGarment_rustSand"
	#define HEADGEAR_TANK "H_Simc_CVC_G_low", "lxWS_H_Tank_tan_F", "H_Simc_pasgt_m81_SWDG_low_b", "H_ShemagOpen_tan", "H_turban_02_mask_snake_lxws", "CUP_H_RUS_Altyn_Goggles_black", "H_turban_02_mask_black_lxws", "H_Simc_CVC_G", "H_Simc_CVC", "H_turban_02_mask_hex_lxws", "CUP_H_RUS_Ratnik_Balaclava_6M2_Desert_4", "CUP_H_Ger_Beret_TankCommander_Blk"
	#define FACEWEAR_TANK "G_AirPurifyingRespirator_02_black_F", "G_AirPurifyingRespirator_02_sand_F", "G_AirPurifyingRespirator_02_olive_F"
	#define VEST_TANK "V_Simc_vest_fauf_2", "CUP_V_B_BAF_DDPM_Osprey_Mk3_Crewman", "CUP_V_B_Armatus_Black", "CUP_V_B_Interceptor_Base_DCU", "CUP_V_B_Armatus_BB_OD", "tmtm_v_modularVestLite_tan", "V_Simc_vest_pasgt_grun"
	#define BACKPACK_TANK "B_AssaultPack_blk", "B_CivilianBackpack_01_Everyday_Black_F", "B_AssaultPack_desert_lxWS"
	#define BACKPACK_TANK_RADIO "B_RadioBag_01_black_F", "tmtm_b_predatorRadio_MTPnoflag", "TFAR_mr3000_multicam"

	#define MEDICAL_STANDARD {"ACE_quikclot",8},{"ACE_morphine",2},{"ACE_tourniquet",4},{"ACE_splint",1},{"ACE_epinephrine",2}
	#define BACKPACK_STANDARD_ITEMS {"ToolKit", 1}
	#define LINKED_ITEMS_STANDARD "ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch","nomex_4_fold_addon"
	#define LINKED_ITEMS_LEADERSHIP "ItemMap","ItemcTab","TFAR_anprc152","ItemCompass","ItemWatch","nomex_4_fold_addon"

	#define TANK_PRIMARY_GREASEGUN {"CUP_smg_M3A1_snd","","","",{"CUP_30Rnd_45ACP_M3A1_SND_M",30},{},""}
	#define TANK_PRIMARY_VERMIN {"SMG_01_F","","acc_flashlight_smg_01","optic_Holosight_smg",{"30Rnd_45ACP_Mag_SMG_01",25},{},""}
	#define TANK_PRIMARY_SA61 {"CUP_smg_SA61_RIS","","","optic_Yorris",{"CUP_20Rnd_B_765x17_Ball_M",20},{},""}
	#define TANK_PRIMARY_SAWEDOFF {"sgun_HunterShotgun_01_sawedoff_F","","","",{"CUP_2Rnd_12Gauge_Slug",2},{},""}
	#define TANK_PRIMARY_AKSU {"CUP_arifle_AKS74U","","","",{"CUP_30Rnd_545x39_AK_M",30},{},""}

	class I_crew_F {
		class GREASE_GUN { 
		// Requires the following DLC:
		// Apex
		// Contact Platform
		displayName = "tank crewman";

		primaryWeapon[] = TANK_PRIMARY_GREASEGUN; 
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};


		uniformClass[] = {UNIFORM_TANK}; 
		headgearClass[] = {HEADGEAR_TANK}; 
		facewearClass[] = {FACEWEAR_TANK}; 
		vestClass[] = {VEST_TANK}; 
		backpackClass[] = {BACKPACK_TANK}; 

		linkedItems[] = {LINKED_ITEMS_STANDARD};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_45ACP_M3A1_SND_M",2,30},{"SmokeShell",2,1}};
		backpackItems[] = {BACKPACK_STANDARD_ITEMS}; 

		basicMedUniform[] = {MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class VERMIN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_VERMIN;
		vestItems[] = {{"30Rnd_45ACP_Mag_SMG_01",2,25},{"SmokeShell",2,1}};

	}; 
	class SA61:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SA61;
		vestItems[] = {{"CUP_20Rnd_B_765x17_Ball_M",2,20}, {"SmokeShell",2,1}};
	};
	class SHOTGUN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SAWEDOFF;
		vestItems[] = {{"SmokeShell",2,1},{"CUP_2Rnd_12Gauge_Slug",10,2}};
	};
	class AKSU: GREASE_GUN 
	{
		primaryWeapon[] = TANK_PRIMARY_AKSU; 
		vestItems[] = {{"SmokeShell",2,1},{"CUP_30Rnd_545x39_AK_M",2,30}};
	}
	};  

class I_Soldier_TL_F {
		class GREASE_GUN { 
		// Requires the following DLC:
		// Apex
		// Contact Platform
		displayName = "crew commander";

		primaryWeapon[] = TANK_PRIMARY_GREASEGUN; 
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Binocular";

		uniformClass[] = {UNIFORM_TANK}; 
		headgearClass[] = {HEADGEAR_TANK}; 
		facewearClass[] = {FACEWEAR_TANK}; 
		vestClass[] = {VEST_TANK}; 
		backpackClass[] = {BACKPACK_TANK_RADIO}; 

		linkedItems[] = {LINKED_ITEMS_LEADERSHIP};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_45ACP_M3A1_SND_M",2,30},{"SmokeShell",2,1}};
		backpackItems[] = {BACKPACK_STANDARD_ITEMS}; 

		basicMedUniform[] = {MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class VERMIN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_VERMIN;
		vestItems[] = {{"30Rnd_45ACP_Mag_SMG_01",2,25},{"SmokeShell",2,1}};

	}; 
	class SA61:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SA61;
		vestItems[] = {{"CUP_20Rnd_B_765x17_Ball_M",2,20}, {"SmokeShell",2,1}};
	};
	class SHOTGUN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SAWEDOFF;
		vestItems[] = {{"SmokeShell",2,1},{"CUP_2Rnd_12Gauge_Slug",10,2}};
	};
	class AKSU: GREASE_GUN 
	{
		primaryWeapon[] = TANK_PRIMARY_AKSU; 
		vestItems[] = {{"SmokeShell",2,1},{"CUP_30Rnd_545x39_AK_M",2,30}};
	}
	};  

	class I_officer_F {
		class GREASE_GUN { 
		// Requires the following DLC:
		// Apex
		// Contact Platform
		displayName = "platoon commander";

		primaryWeapon[] = TANK_PRIMARY_GREASEGUN; 
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Binocular";

		uniformClass[] = {UNIFORM_TANK}; 
		headgearClass[] = {HEADGEAR_TANK}; 
		facewearClass[] = {FACEWEAR_TANK}; 
		vestClass[] = {VEST_TANK}; 
		backpackClass[] = {BACKPACK_TANK_RADIO}; 

		linkedItems[] = {LINKED_ITEMS_LEADERSHIP};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_45ACP_M3A1_SND_M",2,30},{"SmokeShell",2,1}};
		backpackItems[] = {BACKPACK_STANDARD_ITEMS}; 

		basicMedUniform[] = {MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class VERMIN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_VERMIN;
		vestItems[] = {{"30Rnd_45ACP_Mag_SMG_01",2,25},{"SmokeShell",2,1}};

	}; 
	class SA61:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SA61;
		vestItems[] = {{"CUP_20Rnd_B_765x17_Ball_M",2,20}, {"SmokeShell",2,1}};
	};
	class SHOTGUN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SAWEDOFF;
		vestItems[] = {{"SmokeShell",2,1},{"CUP_2Rnd_12Gauge_Slug",10,2}};
	};
	class AKSU: GREASE_GUN 
	{
		primaryWeapon[] = TANK_PRIMARY_AKSU; 
		vestItems[] = {{"SmokeShell",2,1},{"CUP_30Rnd_545x39_AK_M",2,30}};
	}
	}; 

	class I_E_Crew_F {
		class GREASE_GUN { 
		// Requires the following DLC:
		// Apex
		// Contact Platform
		displayName = "nyx crewman";

		primaryWeapon[] = TANK_PRIMARY_GREASEGUN; 
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
	binocular = "Rangefinder";
		 

		uniformClass[] = {UNIFORM_TANK}; 
		headgearClass[] = {HEADGEAR_TANK}; 
		facewearClass[] = {FACEWEAR_TANK}; 
		vestClass[] = {VEST_TANK}; 
		backpackClass[] = {BACKPACK_TANK}; 

		linkedItems[] = {LINKED_ITEMS_STANDARD};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_45ACP_M3A1_SND_M",2,30},{"SmokeShell",2,1}};
		backpackItems[] = {BACKPACK_STANDARD_ITEMS}; 

		basicMedUniform[] = {MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class VERMIN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_VERMIN;
		vestItems[] = {{"30Rnd_45ACP_Mag_SMG_01",2,25},{"SmokeShell",2,1}};

	}; 
	class SA61:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SA61;
		vestItems[] = {{"CUP_20Rnd_B_765x17_Ball_M",2,20}, {"SmokeShell",2,1}};
	};
	class SHOTGUN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SAWEDOFF;
		vestItems[] = {{"SmokeShell",2,1},{"CUP_2Rnd_12Gauge_Slug",10,2}};
	};
	class AKSU: GREASE_GUN 
	{
		primaryWeapon[] = TANK_PRIMARY_AKSU; 
		vestItems[] = {{"SmokeShell",2,1},{"CUP_30Rnd_545x39_AK_M",2,30}};
	}
	};   

	class I_E_Soldier_TL_F {
		class GREASE_GUN { 
		// Requires the following DLC:
		// Apex
		// Contact Platform
		displayName = "nyx crew commander";

		primaryWeapon[] = TANK_PRIMARY_GREASEGUN; 
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Laserdesignator"; 
		

		uniformClass[] = {UNIFORM_TANK}; 
		headgearClass[] = {HEADGEAR_TANK}; 
		facewearClass[] = {FACEWEAR_TANK}; 
		vestClass[] = {VEST_TANK}; 
		backpackClass[] = {BACKPACK_TANK_RADIO}; 

		linkedItems[] = {LINKED_ITEMS_LEADERSHIP};

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_45ACP_M3A1_SND_M",2,30},{"SmokeShell",2,1}};
		backpackItems[] = {BACKPACK_STANDARD_ITEMS}; 

		basicMedUniform[] = {MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class VERMIN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_VERMIN;
		vestItems[] = {{"30Rnd_45ACP_Mag_SMG_01",2,25},{"SmokeShell",2,1}};

	}; 
	class SA61:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SA61;
		vestItems[] = {{"CUP_20Rnd_B_765x17_Ball_M",2,20}, {"SmokeShell",2,1}};
	};
	class SHOTGUN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SAWEDOFF;
		vestItems[] = {{"SmokeShell",2,1},{"CUP_2Rnd_12Gauge_Slug",10,2}};
	};
	class AKSU: GREASE_GUN 
	{
		primaryWeapon[] = TANK_PRIMARY_AKSU; 
		vestItems[] = {{"SmokeShell",2,1},{"CUP_30Rnd_545x39_AK_M",2,30}};
	}
	};  

	class I_E_Officer_F {
		class GREASE_GUN { 
		// Requires the following DLC:
		// Apex
		// Contact Platform
		displayName = "nyx platoon commander";

		primaryWeapon[] = TANK_PRIMARY_GREASEGUN; 
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Laserdesignator"; 

		uniformClass[] = {UNIFORM_TANK}; 
		headgearClass[] = {HEADGEAR_TANK}; 
		facewearClass[] = {FACEWEAR_TANK}; 
		vestClass[] = {VEST_TANK}; 
		backpackClass[] = {BACKPACK_TANK_RADIO}; 

		linkedItems[] = {"ItemMap","I_UavTerminal","ItemRadio","ItemCompass","ItemWatch","nomex_4_fold_addon"}; 

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_45ACP_M3A1_SND_M",2,30},{"SmokeShell",2,1}};
		backpackItems[] = {BACKPACK_STANDARD_ITEMS}; 

		basicMedUniform[] = {MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class VERMIN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_VERMIN;
		vestItems[] = {{"30Rnd_45ACP_Mag_SMG_01",2,25},{"SmokeShell",2,1}};

	}; 
	class SA61:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SA61;
		vestItems[] = {{"CUP_20Rnd_B_765x17_Ball_M",2,20}, {"SmokeShell",2,1}};
	};
	class SHOTGUN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SAWEDOFF;
		vestItems[] = {{"SmokeShell",2,1},{"CUP_2Rnd_12Gauge_Slug",10,2}};
	};
	class AKSU: GREASE_GUN 
	{
		primaryWeapon[] = TANK_PRIMARY_AKSU; 
		vestItems[] = {{"SmokeShell",2,1},{"CUP_30Rnd_545x39_AK_M",2,30}};
	}
	}; 

	
	class I_Soldier_repair_F {
		class GREASE_GUN { 
		// Requires the following DLC:
		// Apex
		// Contact Platform
		displayName = "repair officer";

		primaryWeapon[] = TANK_PRIMARY_GREASEGUN; 
		secondaryWeapon[] = {};
        handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Laserdesignator"; 

		uniformClass[] = {UNIFORM_TANK}; 
		headgearClass[] = {HEADGEAR_TANK}; 
		facewearClass[] = {FACEWEAR_TANK}; 
		vestClass[] = {VEST_TANK}; 
		backpackClass[] = {BACKPACK_TANK_RADIO}; 

		linkedItems[] = {LINKED_ITEMS_LEADERSHIP}; 

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_45ACP_M3A1_SND_M",2,30},{"SmokeShell",2,1}};
		backpackItems[] = {BACKPACK_STANDARD_ITEMS}; 

		basicMedUniform[] = {MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class VERMIN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_VERMIN;
		vestItems[] = {{"30Rnd_45ACP_Mag_SMG_01",2,25},{"SmokeShell",2,1}};

	}; 
	class SA61:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SA61;
		vestItems[] = {{"CUP_20Rnd_B_765x17_Ball_M",2,20}, {"SmokeShell",2,1}};
	};
	class SHOTGUN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SAWEDOFF;
		vestItems[] = {{"SmokeShell",2,1},{"CUP_2Rnd_12Gauge_Slug",10,2}};
	};
	class AKSU: GREASE_GUN 
	{
		primaryWeapon[] = TANK_PRIMARY_AKSU; 
		vestItems[] = {{"SmokeShell",2,1},{"CUP_30Rnd_545x39_AK_M",2,30}};
	}
	}; 

	class I_E_Soldier_Repair_F {
		class GREASE_GUN { 
		// Requires the following DLC:
		// Apex
		// Contact Platform
		displayName = "repair soldier";

		primaryWeapon[] = TANK_PRIMARY_GREASEGUN; 
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};


		uniformClass[] = {UNIFORM_TANK}; 
		headgearClass[] = {HEADGEAR_TANK}; 
		facewearClass[] = {FACEWEAR_TANK}; 
		vestClass[] = {VEST_TANK}; 
		backpackClass[] = {BACKPACK_TANK}; 

		linkedItems[] = {LINKED_ITEMS_LEADERSHIP}; 

		uniformItems[] = {};
		vestItems[] = {{"CUP_30Rnd_45ACP_M3A1_SND_M",2,30},{"SmokeShell",2,1}};
		backpackItems[] = {BACKPACK_STANDARD_ITEMS}; 

		basicMedUniform[] = {MEDICAL_STANDARD};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
	};
	class VERMIN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_VERMIN;
		vestItems[] = {{"30Rnd_45ACP_Mag_SMG_01",2,25},{"SmokeShell",2,1}};

	}; 
	class SA61:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SA61;
		vestItems[] = {{"CUP_20Rnd_B_765x17_Ball_M",2,20}, {"SmokeShell",2,1}};
	};
	class SHOTGUN:  GREASE_GUN
	{
		primaryWeapon[] = TANK_PRIMARY_SAWEDOFF;
		vestItems[] = {{"SmokeShell",2,1},{"CUP_2Rnd_12Gauge_Slug",10,2}};
	};
	class AKSU: GREASE_GUN 
	{
		primaryWeapon[] = TANK_PRIMARY_AKSU; 
		vestItems[] = {{"SmokeShell",2,1},{"CUP_30Rnd_545x39_AK_M",2,30}};
	}
	};        
};