// CfgRespawnTemplates.hpp
// Use this file to define any custom respawn templates to use in your mission 

class FR_TFAR_Spectator
{
    onPlayerKilled = "FR_fnc_spectator_setupSpectator";
    onPlayerRespawn = "FR_fnc_spectator_setupSpectator";
};