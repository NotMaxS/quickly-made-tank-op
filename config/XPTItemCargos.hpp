// XPTItemCargos.hpp
// Used for defining ammo box and vehicle item cargos
// XPTVehicleLoadouts can pull definitions from here in order to supply vehicles
// Supports sub-class randomization, if a definition has multiple sub-classes, the script will automatically select one of them to apply to the vehicle/box.

class itemCargos
{
	class kuma 
	{
		items[] = {	
			{"SmokeShell", 8},
			{"ToolKit",2}, 
			{"B_AssaultPack_desert_lxWS", 2}			
		}; 

		itemsBasicMed[] = {
			{"ACE_fieldDressing", 8},
			{"ACE_epinephrine", 2},
			{"ACE_morphine", 2},  
			{"ACE_tourniquet",4},
			{"ACE_splint",1}
		};
	}; 

	class reconNyx 
	{
		items[] = {	
			{"SmokeShell", 8},
			{"ToolKit",1}, 
			{"B_AssaultPack_desert_lxWS", 1}, 
			{"ACE_UAVBattery", 5}, 
			{"I_UavTerminal", 1}			
		}; 

		itemsBasicMed[] = {
			{"ACE_fieldDressing", 8},
			{"ACE_epinephrine", 2},
			{"ACE_morphine", 2},  
			{"ACE_tourniquet",4},
			{"ACE_splint",1}
		};
	}; 
	

	class logiVehicle 
	{
		items[] = {	
			{"SmokeShell", 20},
			{"ToolKit",4}, 
			{"B_AssaultPack_desert_lxWS", 4}, 
			{"ACE_UAVBattery", 30}			
		}; 

		itemsBasicMed[] = {
			{"ACE_fieldDressing", 30},
			{"ACE_epinephrine", 15},
			{"ACE_morphine", 15},  
			{"ACE_tourniquet",20},
			{"ACE_splint",20}, 
			{"ACE_bloodIV",10},
			{"ACE_bloodIV_500",10}
		};
	}; 
};