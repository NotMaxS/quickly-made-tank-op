// XPTCuratorLoadouts.hpp
// Used for defining advanced loadouts for Zeus spawned units (works almost the same as XPTLoadouts)
// Use XPT_fnc_exportInventory to get correct loadout format, picture guide can be found here: https://imgur.com/a/GrDaJNZ, courtesy of O'Mally
// Supports applying loadouts to crew in vehicles as long as crew classname matches the one defined in here
// Default behaviour is to check if the Zeus placed unit has a special loadout defined. Otherwise, it will use the default loadout as normal
// Advanced functionality allows mission creator to define exactly where items are placed in unit inventories
// Also supports sub-loadout randomization. If a loadout has sub-classes defined, the script will automatically select one of them to apply to the unit.
class curatorLoadouts
{
	
	
	class example_random
	{
		displayName = "Random Loadouts";
		class random_1
		{
			// Loadout info goes here
		};
		class random_2
		{
			// Loadout info goes here
		};
	};
	
	class B_crew_F {
		// Requires the following DLC:
		// Contact Platform
		// Reaction Forces
		displayName = "B_crew_F";

		class subclass_1 {
		primaryWeapon[] = {"CUP_arifle_SR3M_Vikhr_VFG","","","CUP_optic_OKP_7",{"CUP_30Rnd_9x39_SP5_VIKHR_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Binocular";

		uniformClass = "tmtm_u_granit_atacsAu";
		headgearClass = "H_HelmetHeavy_Simple_Sand_RF";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_tan";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"CUP_30Rnd_9x39_SP5_VIKHR_M",1,30}};
		vestItems[] = {};
		backpackItems[] = {{"CUP_30Rnd_9x39_SP5_VIKHR_M",1,30}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_epinephrine",1},{"ACE_morphine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 

		class subclass_2 
		{
			primaryWeapon[] = {"CUP_sgun_SPAS12","","","",{"CUP_8Rnd_12Gauge_Slug",8},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Binocular";

		uniformClass = "tmtm_u_granit_grey";
		headgearClass = "H_PilotHelmetHeli_Black_RF";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_grey";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"SmokeShell",2,1}};
		vestItems[] = {};
		backpackItems[] = {{"CUP_6Rnd_12Gauge_Slug",4,6}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_epinephrine",1},{"ACE_morphine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 
		class subclass_3
		{
			primaryWeapon[] = {"arifle_ash12_desert_RF","","","",{"10Rnd_127x55_Mag_desert_RF",10},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Binocular";

		uniformClass = "CUP_U_O_RUS_Gorka_Green_gloves_kneepads";
		headgearClass = "H_HelmetSpecO_blk";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_black";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {{"SmokeShell",2,1},{"10Rnd_127x55_Mag_desert_RF",1,10}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_epinephrine",1},{"ACE_morphine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 

		class subclass_4 
		{
			primaryWeapon[] = {"SMG_02_F","CUP_muzzle_fh_MP5","","optic_Holosight_smg_blk_F",{"30Rnd_9x21_Mag_SMG_02",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};

		uniformClass = "U_lxWS_Djella_02_Sand";
		headgearClass = "H_HelmetHeavy_VisorUp_Sand_RF";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_tan";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {{"30Rnd_9x21_Mag",1,30},{"SmokeShell",2,1}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_morphine",1},{"ACE_epinephrine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 

		class subclass_5 
		{
			primaryWeapon[] = {"CUP_sgun_slamfire","","","",{"CUP_1Rnd_12Gauge_Slug",1},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};

		uniformClass = "U_lxWS_Djella_03_Green";
		headgearClass = "H_PilotHelmetHeli_MilGreen_RF";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_ranger";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {{"SmokeShell",2,1},{"CUP_1Rnd_12Gauge_Slug",14,1}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_morphine",1},{"ACE_epinephrine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 
	};

	class B_Soldier_F {
		// Requires the following DLC:
		// Contact Platform
		// Reaction Forces
		displayName = "B_crew_F";

		class subclass_1 {
		primaryWeapon[] = {"CUP_arifle_SR3M_Vikhr_VFG","","","CUP_optic_OKP_7",{"CUP_30Rnd_9x39_SP5_VIKHR_M",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Binocular";

		uniformClass = "tmtm_u_granit_atacsAu";
		headgearClass = "H_HelmetHeavy_Simple_Sand_RF";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_tan";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"CUP_30Rnd_9x39_SP5_VIKHR_M",1,30}};
		vestItems[] = {};
		backpackItems[] = {{"CUP_30Rnd_9x39_SP5_VIKHR_M",1,30}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_epinephrine",1},{"ACE_morphine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 

		class subclass_2 
		{
			primaryWeapon[] = {"CUP_sgun_SPAS12","","","",{"CUP_8Rnd_12Gauge_Slug",8},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Binocular";

		uniformClass = "tmtm_u_granit_grey";
		headgearClass = "H_PilotHelmetHeli_Black_RF";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_grey";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {{"SmokeShell",2,1}};
		vestItems[] = {};
		backpackItems[] = {{"CUP_6Rnd_12Gauge_Slug",4,6}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_epinephrine",1},{"ACE_morphine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 
		class subclass_3
		{
			primaryWeapon[] = {"arifle_ash12_desert_RF","","","",{"10Rnd_127x55_Mag_desert_RF",10},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};
		binocular = "Binocular";

		uniformClass = "CUP_U_O_RUS_Gorka_Green_gloves_kneepads";
		headgearClass = "H_HelmetSpecO_blk";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_black";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","ItemGPS","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {{"SmokeShell",2,1},{"10Rnd_127x55_Mag_desert_RF",1,10}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_epinephrine",1},{"ACE_morphine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 

		class subclass_4 
		{
			primaryWeapon[] = {"SMG_02_F","CUP_muzzle_fh_MP5","","optic_Holosight_smg_blk_F",{"30Rnd_9x21_Mag_SMG_02",30},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};

		uniformClass = "U_lxWS_Djella_02_Sand";
		headgearClass = "H_HelmetHeavy_VisorUp_Sand_RF";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_tan";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {{"30Rnd_9x21_Mag",1,30},{"SmokeShell",2,1}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_morphine",1},{"ACE_epinephrine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 

		class subclass_5 
		{
			primaryWeapon[] = {"CUP_sgun_slamfire","","","",{"CUP_1Rnd_12Gauge_Slug",1},{},""};
		secondaryWeapon[] = {};
		handgunWeapon[] = {"Sashka_Russian","","","",{},{},""};

		uniformClass = "U_lxWS_Djella_03_Green";
		headgearClass = "H_PilotHelmetHeli_MilGreen_RF";
		facewearClass = "G_RegulatorMask_F";
		vestClass = "tmtm_v_paca_ranger";
		backpackClass = "B_CombinationUnitRespirator_01_F";

		linkedItems[] = {"ItemMap","","ItemRadio","ItemCompass","ItemWatch",""};

		uniformItems[] = {};
		vestItems[] = {};
		backpackItems[] = {{"SmokeShell",2,1},{"CUP_1Rnd_12Gauge_Slug",14,1}};

		basicMedUniform[] = {{"ACE_quikclot",4},{"ACE_morphine",1},{"ACE_epinephrine",1}};
		basicMedVest[] = {};
		basicMedBackpack[] = {};
		}; 
	};


};