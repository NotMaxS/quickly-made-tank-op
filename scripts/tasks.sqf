// Script for creating player tasks
// Only to be run on the server. BIS_fnc_taskCreate is global.
if (!isServer) exitWith {};

//tasks to plant the jammers
[
	[true, "zeus1", "zeus2"],
	["mainObj"],
	["", "Deactivate the Device", ""],
	[taskPosHelper],
	"CREATED",
	10,
	true,
	"intel",
	false
] call BIS_fnc_taskCreate;

