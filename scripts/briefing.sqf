// Script to handle initial mission briefings
// General guidelines would be to include briefings for the following
// Situation, Mission, and Assets
// Briefings are listed in the opposite order that they are written below. New diaryRecords are always placed at the top of the list.
// https://community.bistudio.com/wiki/createDiaryRecord


if (player isKindOf "VirtualCurator_F")then {
		player createDiaryRecord ["Diary", ["Zeus Notes", 
		"For enemy types, use NATO (regular) Slammers, Rhinos, Marshalls, Striders and Hunters. Make sure NOT to use NATO Desert/Pacific/Woodland due to
		preset loadouts. Don't spawn anymore Boxers. For immersion and consistency, if possible, remove slat cages and equip desert
		camo nets on spawn. Set Striders to NATO camouflage. Do your best to spawn units only inside the white domes inside the Reinforcement Zones
		If players take a Reinforcement Zone, don't spawn anymore vehicles from it. Don't hesitate to counter-attack playerfor if you have to.
		"]];
	};

player createDiaryRecord ["Diary", ["Assets", "Asset List:<br/>
	- 4x Autocannon Nyx <br/>
	- 2x HAT Nyx<br/>
	- 2x Recon Nyx<br/>
	- 6x Kuma (no thermals)<br/>
	- 2x Panther Logistics vehicles (spare tracks, rearm, refuel and repair)<br/> 
	- 8x Darter UAVs (spawn with Nyx platoons) 
"]];

player createDiaryRecord ["Diary", ["Enemies",
"Our intel about the Steel Eagles suggests that they are using M2A1 and M2A4 Slammers as well as Rhino MGSs as primary punching power.
Support vehicles are Marshall variants. Recon vehicles consist of HMG and GMG Striders, Hunters and Boxers."
]];

player createDiaryRecord ["Diary", ["Mission Mechanics",
"1. Darter UAVs will be attached to the HAT and Recon Nyx crews, additional drone batteries will be with the Logistics Teams.<br/>
2. Your PMC Premium Healthcare and Benefits Package does NOT cover the effects of prolonged exposure to the outside elements. If your 
vehicle is lost and if you are not able to quickly find a replacement, the Logistics vehicles will have an Extract hold action on them
that will allow you to become a spectator.<br/>
3. For Dynasim purposes, view-distance for everyone has been set at 2000m.<br/>
4. Repair specialists will be the only ones able to Full Repair vehicles and only when beside the Panther Repair Vehicles."
]];

player createDiaryRecord ["Diary", ["Mission",
"Our forces have been deployed with intent to send out scouts first to locate enemy vehicles and allow follow up tank platoons 
to destroy them. Aside from static positions, enemies are expected to be deploying QRF from white domes inside the marked Reinforcement Zones. Taking
those Zones is a great way to deny the enemy those reinforcements. Once you take the town Lawan Aridi, find the white dome with the 
Ecologists' device inside it and complete the Hold Action to disable whatever it is they are doing."
]];

player createDiaryRecord ["Diary", ["Situation",
"In this dark timeline, the pollution and radiation left behind by the Old Wars has left earth quasi-uninhabitable. Survivors struggle
for resources and power is held by those with the money and resources to hire PMCs. Guns for hire who dominate the market for violence
in today's world. The brutal atmosphere has forced warfare to evolve, being primarily conducted by armoured vehicles; metal coffins on wheels 
impervious to the hostile outdoor conditions that would kill an infantryman in less than half an hour.<br/> 
The vast, arid territory of Kujari is claimed by the Great Khan 
and the Khan controls it with an iron fist. Recently, a group of Ecologists has began intruding on his kingdom with the help of 
PMC group, 'Steel Eagles'. The Great Khan has hired us, PMC Group 'Crimson Kings', to decimate these intruders and destroy their
work."
]];


